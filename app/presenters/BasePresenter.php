<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model;

/**
 * Base presenter for all application presenters.
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */

abstract class BasePresenter extends Nette\Application\UI\Presenter {

    // Use Autowire addon
    use \Kdyby\Autowired\AutowireProperties;
    use \Kdyby\Autowired\AutowireComponentFactories;
    
    // Logout method
    public function actionOut() {
        $this->getUser()->logout();
        $this->flashMessage('Byly jste úspěšně odhlášeni!', 'alert alert-success');
        $this->redirect('Homepage:');
    }

}
