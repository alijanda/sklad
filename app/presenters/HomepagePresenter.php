<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model;

/**
 * Homepage presenter.
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */

class HomepagePresenter extends BasePresenter {

    function startup() {
        parent::startup();
        if ($this->user->isLoggedIn()) {
            
        } else {
            $this->redirect("Sign:in");
        }
    }

    
}
