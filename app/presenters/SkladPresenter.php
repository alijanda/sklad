<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model,
    Nette\Application\UI\Form,
    Kdyby\BootstrapFormRenderer\BootstrapRenderer,
    PdfResponse\PdfResponse,
    Nette\Http\FileUpload,
    Nette\Utils\Strings,
    Nette\Image,
    Nette\Forms\Controls\UploadControl;

/**
 * Sklad presenter.
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */
class SkladPresenter extends HomepagePresenter {

    /**
     * @autowire
     * @var \App\Model\SkladModel
     */
    protected $sklad;

    /**
     * @autowire
     * @var \App\Model\PrvkyModel
     */
    protected $prvky;

    public function beforeRender() {
        $this->template->sklad = $this->sklad->findAll();
        $this->template->prvky = $this->prvky->findAll();
    }

    // zobrazeni vsech
    public function actionVse() {
        $this->template->prvkyV = $this->prvky->findAll();
    }

    // Akce na zobrazeni prvku dane kategorie
    public function actionZobrazPrvek($id) {
        $prvky = $this->prvky->findAll()->where("id", $id);
        if (!$prvky->count()) {
            $this->flashMessage("Prvek nenalezen!", "alert alert-success");
            $this->redirect("this");
        }
        $prvky = $prvky->fetch();
        $this->template->prvkya = $prvky;
    }

    // Signal pro smazani zaznamu ve skladu
    public function handleSmazatPolozku($id) {
        // Najdeme zaznam
        $polozka = $this->sklad->findAll()->where("id", $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$polozka->count()) {
            $this->flashMessage("Položka nenalezena!", "alert alert-success");
            $this->redirect("this");
        }
        // Nacteme data
        $polozka = $polozka->fetch();
        // Smazeme
        $polozka->delete();
        // Vypiseme info a presmerujeme
        $this->flashMessage("Položka byla úspěšně odstřaněna!", "alert alert-success");
        $this->redirect("this");
    }

    // Signal pro smazani skupiny ve skladu
    public function handleSmazatSkupinu($id) {
        // Najdeme zaznam
        $skupina = $this->prvky->findAll()->where("id", $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$skupina->count()) {
            $this->flashMessage("Prvek nenalezen!", "alert alert-success");
            $this->redirect("this");
        }
        // Nacteme data
        $skupina = $skupina->fetch();
        // Smazeme
        $skupina->delete();
        // Vypiseme info a presmerujeme
        $this->flashMessage("Prvek byl úspěšně odstřaněn!", "alert alert-success");
        $this->redirect("this");
    }

    // Akce na upravu zaznamu skladu
    public function actionUpravit($id) {
        // Najdeme zaznam
        $sklad = $this->sklad->findAll()->where('id', $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$sklad->count()) {
            $this->flashMessage("Položka nenalezena!", "alert alert-success");
            $this->redirect("default");
        }
        // Nacteme data
        $sklad = $sklad->fetch();
        // Nastavime defaultni hodnoty do formulare a nahradime button
        $this["prvekForm"]->setDefaults($sklad);
        $this["prvekForm"]['send']->caption = "Uložit";
        // Dostaname data do sablony
        $this->template->sklads = $sklad;
    }

    // Akce pro zobrazeni prvku
    public function actionZobrazit($id) {
        // Najdeme zaznam
        $prvek = $this->sklad->findAll()->where("id", $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$prvek->count()) {
            $this->flashMessage("Prvek nenalezen!", "alert alert-success");
            $this->redirect("default");
        }
        // Nacteme data
        $prvek = $prvek->fetch();
        // Dostaname data do sablony
        $this->template->prvek = $prvek;
    }

    // Tvorba PDF
    public function actionPdf($id) {
        // Najdeme zaznam
        $prvek = $this->sklad->findAll()->where("id", $id)->fetch();
        // Nastavime sablony
        $template = $this->createTemplate()->setFile(__DIR__ . "/../templates/pdf/prvek.latte");
        $template->nazev_cz = $prvek->nazev;
        $template->nazev_eng = $prvek->nazev_eng;
        $template->rozmery = $prvek->rozmery;
        $template->vaha = $prvek->vaha;
        $template->prikon = $prvek->prikon;
        $template->cena = $prvek->cena;
        $template->cena_pronajmu = $prvek->cena_pronajmu;
        $template->pocet = $prvek->pocet;
        $template->poznamka = $prvek->poznamka;
        $template->zobrazovat = $prvek->zobrazit;
        $template->obrazek = $prvek->obrazek;

        $pdf = new PDFResponse($template);

        // Všechny tyto konfigurace jsou nepovinné:
        // Orientace stránky
        $pdf->pageOrientaion = PdfResponse::ORIENTATION_PORTRAIT;
        // Formát stránky
        $pdf->pageFormat = "A4";

        // Způsob zobrazení PDF
        $pdf->displayLayout = "continuous";
        // Velikost zobrazení
        $pdf->displayZoom = "fullwidth";

        // Název dokumentu
        $pdf->documentTitle = "Detail prvku -" . $prvek->nazev;
        // Dokument vytvořil:
        $pdf->documentAuthor = "Artao.cz";

        // Ukončíme presenter -> předáme řízení PDFresponse
        $this->sendResponse($pdf);
    }

    // Akce na kopirovani prvku
    public function actionKopirovat($id) {
        // Najdeme zaznam
        $sklad = $this->sklad->findAll()->where('id', $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$sklad->count()) {
            $this->flashMessage("Položka nenalezena!", "alert alert-success");
            $this->redirect("default");
        }
        // Nacteme data
        $sklad = $sklad->fetch();
        // Nastavime defaultni hodnoty do formulare 
        $this["prvekCForm"]->setDefaults($sklad);
        // Dostaname data do sablony
        $this->template->sklads = $sklad;
    }

    // Formular na vytvoreni/upravu skladove polozky
    public function createComponentPrvekForm() {
        $form = new Form();
        // Nacteni skupin
        $skupiny = $this->prvky->findAll()->fetchPairs("id", "nazev_cz");
        // Vytvoreni formulare
        $form->addSelect("skupina_prvku_id", "Skupina", $skupiny)
                ->setPrompt("Zvolte skupinu")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("nazev", "Název")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("nazev_eng", "Název anglicky")
                ->setAttribute('class', 'form-control');
        $form->addUpload("obrazek", "Obrázek")
                ->setAttribute('class', 'form-control');
        $form->addText("rozmery", "Rozměry")
                ->setAttribute('class', 'form-control');
        $form->addText("vaha", "Váha")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("prikon", "Příkon")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("cena", "Cena")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("cena_pronajmu", "Cena pronájmu")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("pocet", "Počet KS")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("poznamka", "Poznámka")
                ->setAttribute('class', 'form-control');
        $form->addCheckbox("zobrazit", "");
        // Odesilaci tlacitko
        $form->addSubmit('send', 'Přidat skladovou položku')
                ->getControlPrototype()
                ->class('btn btn-info');
        // Zavolame metodu usersFormSucceeded()po uspechu
        $form->onSuccess[] = $this->prvekFormSucceeded;
        // Set Twitter Bootstrap render
        $form->setRenderer(new BootstrapRenderer());

        return $form;
    }

    // Zpracovani formulare
    public function prvekFormSucceeded($form) {
        // Nacteme hodnoty z formulare
        $id = $this->getParam("id");
        $values = $form->getValues();
        // Upravime prvek
        if ($id) {
            // Pokud ID existuje, nahradime data
            // Overeni zda je vybran obrazek
            if ($values->obrazek->getName()) {
                // Zpracovani img
                $img_name = Strings::webalize(new \Nette\DateTime);

                // Pohyb souboru a zpracovani
                $img = Image::fromFile($values->obrazek);
                $img->resize(1024, NULL, Image::SHRINK_ONLY);
                $img->toString(\Nette\Image::PNG);
                $img->save("upload/" . $img_name . ".png");

                $this->sklad->findAll()->where('id', $id)->update(array(
                    "skupina_prvku_id" => $values->skupina_prvku_id,
                    "nazev" => $values->nazev,
                    "nazev_eng" => $values->nazev_eng,
                    "rozmery" => $values->rozmery,
                    "vaha" => $values->vaha,
                    "prikon" => $values->prikon,
                    "cena" => $values->cena,
                    "cena_pronajmu" => $values->cena_pronajmu,
                    "pocet" => $values->pocet,
                    "poznamka" => $values->poznamka,
                    "zobrazit" => $values->zobrazit,
                    "obrazek" => "/upload/" . $img_name . ".png"
                ));
            } else {
                unset($values->obrazek);
                $this->sklad->findAll()->where('id', $id)->update(array(
                    "skupina_prvku_id" => $values->skupina_prvku_id,
                    "nazev" => $values->nazev,
                    "nazev_eng" => $values->nazev_eng,
                    "rozmery" => $values->rozmery,
                    "vaha" => $values->vaha,
                    "prikon" => $values->prikon,
                    "cena" => $values->cena,
                    "cena_pronajmu" => $values->cena_pronajmu,
                    "pocet" => $values->pocet,
                    "poznamka" => $values->poznamka,
                    "zobrazit" => $values->zobrazit
                ));
            }
            // Vypíšeme info a přesměrujeme
            $this->flashMessage("Prvek byl úspěšně upraven!", "alert alert-success");
            $this->redirect("Sklad:default");
        } else {
            // Vytvorime novy prvek
            if ($values->obrazek->getName()) {
                // Zpracovani img
                $img_name = Strings::webalize(new \Nette\DateTime);

                // Pohyb souboru a zpracovani
                $img = Image::fromFile($values->obrazek);
                $img->resize(1024, NULL, Image::SHRINK_ONLY);
                $img->toString(\Nette\Image::PNG);
                $img->save("upload/" . $img_name . ".png");

                $this->sklad->insert(array(
                    "skupina_prvku_id" => $values->skupina_prvku_id,
                    "nazev" => $values->nazev,
                    "nazev_eng" => $values->nazev_eng,
                    "rozmery" => $values->rozmery,
                    "vaha" => $values->vaha,
                    "prikon" => $values->prikon,
                    "cena" => $values->cena,
                    "cena_pronajmu" => $values->cena_pronajmu,
                    "pocet" => $values->pocet,
                    "poznamka" => $values->poznamka,
                    "zobrazit" => $values->zobrazit,
                    "obrazek" => "/upload/" . $img_name . ".png"
                ));
            } else {
                unset($values->obrazek);
                $this->sklad->insert(array(
                    "skupina_prvku_id" => $values->skupina_prvku_id,
                    "nazev" => $values->nazev,
                    "nazev_eng" => $values->nazev_eng,
                    "rozmery" => $values->rozmery,
                    "vaha" => $values->vaha,
                    "prikon" => $values->prikon,
                    "cena" => $values->cena,
                    "cena_pronajmu" => $values->cena_pronajmu,
                    "pocet" => $values->pocet,
                    "poznamka" => $values->poznamka,
                    "zobrazit" => $values->zobrazit
                ));
            }
            // Vypíšeme info a přesměrujeme
            $this->flashMessage('Přidání prvku bylo úspěšné!', 'alert alert-success');
            $this->redirect('Sklad:default');
        }
    }

    // Formular na vytvoreni/upravu skladove polozky
    public function createComponentPrvekCForm() {
        $form = new Form();
        // Nacteni skupin
        $skupiny = $this->prvky->findAll()->fetchPairs("id", "nazev_cz");
        // Vytvoreni formulare
        $form->addSelect("skupina_prvku_id", "Skupina", $skupiny)
                ->setPrompt("Zvolte skupinu")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("nazev", "Název")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("nazev_eng", "Název anglicky")
                ->setAttribute('class', 'form-control');
        $form->addUpload("obrazek", "Obrázek")
                ->setAttribute('class', 'form-control');
        $form->addText("rozmery", "Rozměry")
                ->setAttribute('class', 'form-control');
        $form->addText("vaha", "Váha")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("prikon", "Příkon")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("cena", "Cena")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("cena_pronajmu", "Cena pronájmu")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("pocet", "Počet KS")
                ->setRequired('Pole musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText("poznamka", "Poznámka")
                ->setAttribute('class', 'form-control');
        $form->addCheckbox("zobrazit", "");
        // Odesilaci tlacitko
        $form->addSubmit('send', 'Přidat skladovou položku')
                ->getControlPrototype()
                ->class('btn btn-info');
        // Zavolame metodu usersFormSucceeded()po uspechu
        $form->onSuccess[] = $this->prvekCFormSucceeded;
        // Set Twitter Bootstrap render
        $form->setRenderer(new BootstrapRenderer());

        return $form;
    }

    // Zpracovani formulare
    public function prvekCFormSucceeded($form) {
        // Nacteme hodnoty z formulare
        $values = $form->getValues();
        if ($values->obrazek->getName()) {
            // Zpracovani img
            $img_name = Strings::webalize(new \Nette\DateTime);

            // Pohyb souboru a zpracovani
            $img = Image::fromFile($values->obrazek);
            $img->resize(1024, NULL, Image::SHRINK_ONLY);
            $img->toString(\Nette\Image::PNG);
            $img->save("upload/" . $img_name . ".png");

            $this->sklad->insert(array(
                "skupina_prvku_id" => $values->skupina_prvku_id,
                "nazev" => $values->nazev,
                "nazev_eng" => $values->nazev_eng,
                "rozmery" => $values->rozmery,
                "vaha" => $values->vaha,
                "prikon" => $values->prikon,
                "cena" => $values->cena,
                "cena_pronajmu" => $values->cena_pronajmu,
                "pocet" => $values->pocet,
                "poznamka" => $values->poznamka,
                "zobrazit" => $values->zobrazit,
                "obrazek" => "/upload/" . $img_name . ".png"
            ));
        } else {
            unset($values->obrazek);
            $this->sklad->insert(array(
                "skupina_prvku_id" => $values->skupina_prvku_id,
                "nazev" => $values->nazev,
                "nazev_eng" => $values->nazev_eng,
                "rozmery" => $values->rozmery,
                "vaha" => $values->vaha,
                "prikon" => $values->prikon,
                "cena" => $values->cena,
                "cena_pronajmu" => $values->cena_pronajmu,
                "pocet" => $values->pocet,
                "poznamka" => $values->poznamka,
                "zobrazit" => $values->zobrazit
            ));
        }
        // Vypíšeme info a přesměrujeme
        $this->flashMessage('Přidání prvku bylo úspěšné!', 'alert alert-success');
        $this->redirect('Sklad:default');
    }

}
