<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model,
    Nette\Application\UI\Form,
    Nette\Http\FileUpload,
    Nette\Utils\Strings,
    Nette\Forms\Controls\UploadControl,
    Nette\Utils,
    Nette\Http\Url;

/**
 * Rad presenter.
 * Presenter obsluhujici dokumenty a praci s nimi
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */

class RadPresenter extends HomepagePresenter {

    /**
     * @autowire
     * @var \App\Model\DokumentyModel
     */
    protected $doc;

    // Predame $doc do sablony
    public function renderDefault() {
        $this->template->doc = $this->doc->findAll()->order('pozice ASC');
    }
    
    // Signal pro smazani zaznamu
    public function handleSmazat($id) {
        $rad = $this->doc->findAll()->where("id", $id);
        if (!$rad->count()) {
            $this->flashMessage("Soubor nenalezen!", "alert alert-danger");
            $this->redirect("this");
        }
        $rad = $rad->fetch();
        unlink(__DIR__ . "/../../../www/upload/" . $rad->cesta . "." . $rad->koncovka);
        $rad->delete();
        $this->flashMessage("Soubor byl úspěšně odstřaněn!", "alert alert-success");
        $this->redirect("this");
    }
    
    // Signal pro posun o pozici vys a zaroven starou na pozici niz
    public function handleUp($id) {
        $up = $this->doc->findAll()->where('id', $id)->fetch();
        $down = $this->doc->findAll()->where('pozice', $up->pozice - 1)->fetch();
        $up->update(array('pozice' => $down->pozice));
        $down->update(array('pozice' => $down->pozice + 1));
        $this->redirect("this");
    }
    
    // Signal pro posun o pozici niz a zaroven starou na pozici vys
    public function handleDown($id) {
        $down = $this->doc->findAll()->where('id', $id)->fetch();
        $up = $this->doc->findAll()->where('pozice', $down->pozice + 1)->fetch();
        $down->update(array('pozice' => $up->pozice));
        $up->update(array('pozice' => $down->pozice - 1));
        $this->redirect("this");        
    }
    
    // Signal na upravu souboru
    public function actionUpravit($id) {
        $rad = $this->doc->findAll()->where("id", $id);
        if (!$rad->count())
            $this->redirect("default");
        $rad = $rad->fetch();
        
        // Nastavime defaultni hodnoty do formulare a nahradime button
        $this["dokumentForm"]->setDefaults($rad);
        $this["dokumentForm"]['send']->caption = "Uložit změny";
        $this->template->doc = $rad;
    }
    
    // Formular pro nahratí, editaci
    public function createComponentDokumentForm() {
        $form = new Form();
        $form->addUpload('upload', '')
                ->setAttribute('class', 'form-control')
                ->addCondition(Form::FILLED)
                ->addRule(Form::MAX_FILE_SIZE, 'Soubor je příliš velký! Povolená velikost je 2M.', 2 * 1024 * 1024);
        $form->addText('nazev', '')
                ->setAttribute('class', 'form-control');
        $form->addCheckbox('tucny', '');

        // Odesilaci tlacitko
        $form->addSubmit('send', 'Přidat záznam')
                ->getControlPrototype()
                ->class('btn btn-info');

        // Zavolame metodu dokumentFormSucceeded()po uspechu
        $form->onSuccess[] = $this->dokumentFormSucceeded;
        
        return $form;
    }
    
    // Zpracovani vystupu formulare
    public function dokumentFormSucceeded($form) {
        // Nacteme hodnoty z formulare
        $id = $this->getParam("id");
        $values = $form->getValues();
        
        // Pokud je prilozen soubor
        if($values->upload) {
            
            // Rozkouskujeme nazev pro vytazeni pripony
            $typ = $values->upload;
            $typ=explode(".", $typ->name);
            $poc=count($typ);
            $pripona_dokumentu=strtolower($typ[$poc-1]);

            // Prevedeme nazev do nazvu pro DB
            $name = Strings::webalize($values->upload->name);

            // Ulozime soubor 
            $doc = $values->upload;
            $doc->move(__DIR__ . "/../../../www/upload/" . $name . "." . $pripona_dokumentu);
        }
        
        // Pokud zaznam existuje, upravime jej
        if($id) {
            $this->doc->findAll()->where('id', $id)->update(array(
                "nazev" => $values->nazev,
                "tucny" => $values->tucny,
                "datum" => new Utils\DateTime
            ));
            
            // Vypíšeme info a přesměrujeme
            $this->flashMessage('Editace byla úspěšná!', 'alert alert-success');
            $this->redirect('Rad:default');
        } 
        
        // Nebo vytvorime novy zaznam
        else {
            $this->doc->findAll()->insert(array(
                "nazev" => $values->nazev,
                "tucny" => $values->tucny,
                "datum" => new Utils\DateTime,
                "koncovka" => $pripona_dokumentu,
                "cesta" => $name
            ));
            
            // Vypíšeme info a přesměrujeme
            $this->flashMessage('Přidání bylo úspěšné!', 'alert alert-success');
            $this->redirect('Rad:default');
        }
        
    }
}
