<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model,
    Nette\Application\UI\Form,
    Kdyby\BootstrapFormRenderer\BootstrapRenderer;

/**
 * Sign in presenters.
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */

class SignPresenter extends BasePresenter {
    
    // Create login form
    protected function createComponentSignInForm() {
        $form = new Form;
        $form->addText('username', 'Uživatelské jméno:')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Login ...')
                ->setRequired('Please enter your username.');
        $form->addPassword('password', 'Heslo:')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Heslo ...')
                ->setRequired('Please enter your password.');
        $form->addCheckbox('remember', 'Zapamatovat přihlášení');
        $form->addSubmit('send', 'Přihlásit')
                ->getControlPrototype()
                ->class('btn btn-success pull-right');

        // Call method signInFormSucceeded() on success
        $form->onSuccess[] = $this->signInFormSucceeded;
        
        // Set Twitter Bootstrap render
        $form->setRenderer(new BootstrapRenderer());
        
        return $form;
    }

    // Method on success signInForm()
    public function signInFormSucceeded($form) {
        $values = $form->getValues();

        // Check is chacked remember checkbox
        if ($values->remember) {
            $this->getUser()->setExpiration('14 days', FALSE);
        } else {
            $this->getUser()->setExpiration('20 minutes', TRUE);
        }

        // Try login form values
        try {
            $this->getUser()->login($values->username, $values->password);
            $this->flashMessage('Byly jste úspěšně přihlášeni!', 'alert alert-success');
            $this->redirect('Projekty:');
        } catch (\Nette\Security\AuthenticationException $e) {
            $this->flashMessage($e->getMessage(), 'alert alert-warning');
        }
    }

}