<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model,
    Nette\Application\UI\Form;

/**
 * Ucet presenter.
 * Presenter obsluhujici uzivatelsky ucet
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */

class UcetPresenter extends HomepagePresenter {

    /**
     * @autowire
     * @var \App\Model\UsersModel
     */
    protected $ucet;

    // Signal na upravu uzivatele
    public function actionDefault($id) {
        $ucet = $this->ucet->findAll()->where("id", $id);
        if (!$ucet->count())
            $this->redirect("default");
        $ucet = $ucet->fetch();
        
        // Nastavime defaultni hodnoty do formulare a nahradime button
        $this["ucetForm"]->setDefaults($ucet);
        $this->template->ucet = $ucet;
    }

    // Vytvorime formular pro pridani/editaci uzivatelu
    protected function createComponentUcetForm() {
        $form = new Form();
        $form->addText('name', 'Jméno')
                ->setRequired('Jméno musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText('username', 'Uživatelské jméno')
                ->setRequired('Uživatelské jméno musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText('email', 'Email')
                ->setRequired('Email musí být vyplněn!')
                ->setAttribute('class', 'form-control');
        $form->addPassword('password_old', 'Nové heslo')
                ->setRequired('Heslo musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addPassword('password_new', 'Nové heslo')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Nové heslo ...');
        $form->addPassword('password_new2', '')
                ->addRule(Form::EQUAL, "Nová hesla se musí shodovat!", $form["password_new"])
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Opakovat nové heslo ...');
        
        // Odesilaci tlacitko
        $form->addSubmit('send', 'Editovat účet')
                ->getControlPrototype()
                ->class('btn btn-info')
                ->onClick('return dotaz();');

        // Zavolame metodu ucetFormSucceeded()po uspesne validaci
        $form->onSuccess[] = $this->ucetFormSucceeded;

        return $form;
    }

    // Zpracovani formulare uzivatelu
    public function ucetFormSucceeded($form) {
        // Nacteme hodnoty z formulare
        $id = $this->getParam("id");
        $values = $form->getValues();
        
        // Unsetneme 1. nove heslo
        unset($values->password_new);
        
        // Nacteme stare heslo z DB
        $db_pass = $this->ucet->findAll()->where("id", $id);
        $db_pass = $db_pass->fetch()->password;
        
        // Prevedem stare heslo z formulare na md5 hash
        $form_pass = md5($values->password_old);
        
        // Overime, zda heslo z DB se shoduje se starym heslem z formulare
        if($db_pass == $form_pass) {
            
            // Pokud je nastaveno nove heslo
            if($values->password_new2){
                $this->ucet->findAll()->where('id', $id)->update(array(
                    "name" => $values->name,
                    "username" => $values->username,
                    "email" => $values->email,
                    "password" => md5($values->password_new2)
                ));
                
                // Vypiseme success zpravu a presmerujeme
                $this->flashMessage('Heslo úspěšně změněno', 'alert alert-success');
                $this->redirect('Projekty:');                
            } else {
                 $this->ucet->findAll()->where('id', $id)->update(array(
                    "name" => $values->name,
                    "username" => $values->username,
                    "email" => $values->email,
                ));
                
                // Vypiseme success zpravu a presmerujeme
                $this->flashMessage('Údaje úspěšně změněny', 'alert alert-success');
                $this->redirect('Projekty:');   
            }
        } else {
            $this->flashMessage('Staré heslo nesouhlasí!', 'alert alert-danger');   
        }

    }

}
