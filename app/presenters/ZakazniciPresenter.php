<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model,
    Nette\Application\UI\Form,
    Kdyby\BootstrapFormRenderer\BootstrapRenderer,
    Nette\Utils,
    VCardIFL;

/**
 * Zakaznici presenter.
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */

class ZakazniciPresenter extends HomepagePresenter {

    /**
     * @autowire
     * @var \App\Model\ZakazniciModel
     */
    protected $zakaznici;

    // Nacteni $zakaznici do sablony
    public function renderDefault() {
        $this->template->zakaznici = $this->zakaznici->findAll()->order('firma');
    }

    // Dostaneme $zakaznici do pozdejsich sablon
    public function beforeRender() {
        $this->template->zakaznici = $this->zakaznici->findAll()->order('firma');
    }

    // Signal pro naceteni dat z ARESU
    public function handleNactiAres($ic) {

        // Vytazeni dat ze serveru ARES
        define('ARES', 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=');
        $ico = intval($ic);
        if ($curl = curl_init(ARES . $ico)) {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $content = curl_exec($curl);
            curl_close($curl);
            $xml = @simplexml_load_string($content);
        }
        $a = array();
        if (isset($xml)) {
            $ns = $xml->getDocNamespaces();
            $data = $xml->children($ns['are']);
            $el = $data->children($ns['D'])->VBAS;
            if (strval($el->ICO) == $ico) {
                $a['ico'] = strval($el->ICO);
                $a['dic'] = strval($el->DIC);
                $a['firma'] = strval($el->OF);
                if (strval($el->AA->CO)) {
                    $a['ulice'] = strval($el->AA->NU) . ' ' . strval($el->AA->CD) . '/' . strval($el->AA->CO);
                } else {
                    $a['ulice'] = strval($el->AA->NU) . ' ' . strval($el->AA->CD);
                }
                if (strval($el->AA->NCO)) {
                    $a['mesto'] = strval($el->AA->N) . '-' . strval($el->AA->NCO);
                } else {
                    $a['mesto'] = strval($el->AA->N);
                }
                $a['psc'] = strval($el->AA->PSC);
            } else {
                $this->flashMessage('IČ firmy nebylo v databázi ARES nalezeno', 'alert alert-danger');
                $this->redirect('this');
            }
        } else {
            $this->flashMessage('Databáze není dostupná', 'alert alert-danger');
            $this->redirect('this');
        }

        // Nastaveni ziskanych dat do sablony a prepsani inputu formulare
        $this["zakaznikForm"]->setDefaults($a);
    }

    // Akce pro detail zakaznika ve statistice
    public function actionDetail($id) {
        // Najdeme zaznam
        $zakaznici = $this->zakaznici->findAll()->where("id", $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$zakaznici->count())
            $this->redirect("default");
        // Nacteme data
        $zakaznici = $zakaznici->fetch();
        // Dostaname data do sablony
        $this->template->zakaznicia = $zakaznici;
    }

    // Akce pro vyhledani zakaznika
    public function actionHledat($firma, $jmeno, $prijmeni, $mesto, $ic, $dic, $ulice, $web, $pozn) {

        // Pokud je vyplneno, predam do where
        $cond = array();
        if ($firma !== NULL) {
            $cond["firma = ?"] = $firma;
        } elseif ($jmeno !== NULL) {
            $cond["jmeno = ?"] = $jmeno;
        } elseif ($prijmeni !== NULL) {
            $cond["prijmeni = ?"] = $prijmeni;
        } elseif ($mesto !== NULL) {
            $cond["mesto = ?"] = $mesto;
        } elseif ($ic !== NULL) {
            $cond["ic = ?"] = $ic;
        } elseif ($dic !== NULL) {
            $cond["dic = ?"] = $dic;
        } elseif ($ulice !== NULL) {
            $cond["ulice = ?"] = $ulice;
        } elseif ($pozn !== NULL) {
            $cond["pozn = ?"] = $pozn;
        } elseif ($web !== NULL) {
            $cond["web = ?"] = $web;
        }
        // Najdeme zaznam
        $firma = $this->zakaznici->findAll();
        array_unshift($cond, implode(" OR ", array_keys($cond)));
        call_user_func_array(array($firma, 'where'), $cond);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$firma->count()) {
            $this->flashMessage("Zákazník dle parametrů nenalezen!", "alert alert-danger");
            $this->redirect("default");
        }
        // Dostaname data do sablony
        $this->template->hledat = $firma;
    }

    // Signal pro smazani zaznamu zakaznika
    public function handleSmazat($id) {
        // Najdeme zaznam
        $zakaznici = $this->zakaznici->findAll()->where("id", $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$zakaznici->count()) {
            $this->flashMessage("Zákazník nenalezen!", "alert alert-success");
            $this->redirect("this");
        }
        // Nacteme data
        $zakaznici = $zakaznici->fetch();
        // Smazeme
        $zakaznici->delete();
        // Vypiseme info a presmerujeme
        $this->flashMessage("Zákazník byl úspěšně odstřaněn!", "alert alert-success");
        $this->redirect("this");
    }

    // Akce pro zobrazeni zakaznika
    public function actionZobrazit($id) {
        // Najdeme zaznam
        $zakaznici = $this->zakaznici->findAll()->where("id", $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$zakaznici->count()) {
            $this->flashMessage("Zákazník nenalezen!", "alert alert-success");
            $this->redirect("default");
        }
        // Nacteme data
        $zakaznici = $zakaznici->fetch();
        // Dostaname data do sablony
        $this->template->zakaznik = $zakaznici;
    }

    // Akce na upravu zaznamu zakaznika
    public function actionUpravit($id) {
        // Najdeme zaznam
        $zakaznici = $this->zakaznici->findAll()->where('id', $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$zakaznici->count()) {
            $this->flashMessage("Zákazník nenalezen!", "alert alert-success");
            $this->redirect("default");
        }
        // Nacteme data
        $zakaznici = $zakaznici->fetch();
        // Nastavime defaultni hodnoty do formulare a nahradime button
        $this["zakaznikForm"]->setDefaults($zakaznici);
        $this["zakaznikForm"]['send']->caption = "Uložit";
        // Dostaname data do sablony
        $this->template->za = $zakaznici;
    }

    // Signal na vlozeni zakaznika do projektu
    public function actionVlozit($id) {
        // Najdeme zaznam
        $zakaznici = $this->zakaznici->findAll()->where("id", $id);
        // Pokud neni nalezen, presmeruji a vypisu zpravu
        if (!$zakaznici->count()) {
            $this->flashMessage("Zákazník nenalezen!", "alert alert-success");
            $this->redirect("default");
        }
        // Nacteme data
        $zakaznici = $zakaznici->fetch();
        // Nastavime defaultni hodnoty do formulare a nahradime button
        $this["zakazniciForm"]->setDefaults($zakaznici);
        $this["zakazniciForm"]['send']->caption = "Uložit";
        // Dostaname data do sablony
        $this->template->zakaznici = $zakaznici;
    }

    // Vytvorit vCart zakaznika
    public function actionVcart($id) {
        // Najdeme zaznam
        $zakaznici = $this->zakaznici->findAll()->where("id", $id)->fetch();
        // Nastavime pole dat
        $firma = iconv("", "", $zakaznici->firma);
        $titul_pred = iconv("", "", $zakaznici->titul_pred);
        $jmeno = iconv("", "", $zakaznici->jmeno);
        $prijmeni = iconv("", "", $zakaznici->prijmeni);
        $titul_po = iconv("", "", $zakaznici->titul_po);
        $jmeno_vcard = "";
        if ($titul_pred == "") {
            $jmeno_vcard = $jmeno;
        } else {
            $jmeno_vcard = $titul_pred . " " . $jmeno;
        }
        $prijmeni_vcard = "";
        if ($titul_po == "") {
            $prijmeni_vcard = $prijmeni;
        } else {
            $prijmeni_vcard = $prijmeni . " " . $titul_po;
        }
        $ulice = iconv("", "", $zakaznici->ulice);
        $mesto = iconv("", "", $zakaznici->mesto);
        $psc = iconv("", "", $zakaznici->psc);
        $telefon = iconv("", "", $zakaznici->telefon);
        $mail = iconv("", "", $zakaznici->mail);
        $web = iconv("", "", $zakaznici->web);
        // Hledej a nahrad
        $hledej = array('á', 'č', 'ď', 'é', 'ě', 'í', 'ň', 'ó', 'ř', 'ą', '»', 'ú', 'ů', 'ý', 'ľ', 'Á', 'Č', 'Ď', 'É', 'Ě', 'Í', 'Ň', 'Ó', 'Ř', '©', '«', 'Ú', 'Ů', 'Ý', '®', ' ', ',', ')', '(', '"', '%', ':', '>', '<', '!', '?');
        $nahrad = array('a', 'c', 'd', 'e', 'e', 'i', 'n', 'o', 'r', 's', 't', 'u', 'u', 'y', 'z', 'a', 'c', 'd', 'e', 'e', 'i', 'n', 'o', 'r', 's', 't', 'u', 'u', 'y', 'z', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_');
        $filename = str_replace($hledej, $nahrad, $zakaznici->prijmeni);
        $filename = strtolower($filename);
        // Samotna pole
        $dataArray = array(
            "fileName" => "vcard_" . $filename . "", //file name
            "saveTo" => "upload", //upload dir
            "vcard_birtda" => "",
            "vcard_f_name" => "" . $prijmeni_vcard . "",
            "vcard_s_name" => "" . $jmeno_vcard . "",
            "vcard_uri" => "",
            "vcard_nickna" => "",
            "vcard_note" => "",
            "vcard_cellul" => "",
            "vcard_compan" => "" . $firma . "",
            "vcard_p_pager" => "",
            "vcard_h_addr" => "",
            "vcard_h_city" => "",
            "vcard_h_coun" => "",
            "vcard_h_fax" => "",
            "vcard_h_mail" => "" . $mail . "",
            "vcard_h_phon" => "",
            "vcard_h_zip" => "",
            "vcard_h_uri" => "",
            "vcard_w_addr" => "" . $ulice . "",
            "vcard_w_city" => "" . $mesto . "",
            "vcard_w_coun" => "",
            "vcard_w_fax" => "",
            "vcard_w_mail" => "",
            "vcard_w_phon" => "" . $telefon . "",
            "vcard_w_role" => "",
            "vcard_w_titl" => "",
            "vcard_w_zip" => "" . $psc . "",
            "vcard_w_uri" => "" . $web . ""
        );
        // Vytvoreni 1. krok
        $vcard = new VCardIFL($dataArray);
        // Vytvoreni 2. krok
        $vcard->createVcard();
        // Stazeni
        $vcard->DownloadVcard();
        // Ukonceni
        $this->terminate();
    }

    // Formular na hledani zakaznika
    public function createComponentHledatForm() {
        $form = new Form();
        // Vytvorime inputy
        $form->addText('firma', 'Firma')
                ->setAttribute('class', 'form-control');
        $form->addText('jmeno', 'Jméno')
                ->setAttribute('class', 'form-control');
        $form->addText('ic', 'IČ')
                ->setAttribute('class', 'form-control');
        $form->addText('prijmeni', 'Přijmení')
                ->setAttribute('class', 'form-control');
        $form->addText('mesto', 'Město')
                ->setAttribute('class', 'form-control');
        $form->addText('dic', 'DIČ')
                ->setAttribute('class', 'form-control');
        $form->addText('ulice', 'Ulice')
                ->setAttribute('class', 'form-control');
        $form->addText('pozn', 'Poznámka')
                ->setAttribute('class', 'form-control');
        $form->addText('web', 'WEB')
                ->setAttribute('class', 'form-control');
        // Odesilaci tlacitko
        $form->addSubmit('send', 'Hledat')
                ->getControlPrototype()
                ->class('btn btn-info');
        // Zavolame metodu usersFormSucceeded()po uspechu
        $form->onSuccess[] = $this->hledatFormSucceeded;
        // Set Twitter Bootstrap render
        $form->setRenderer(new BootstrapRenderer());
        return $form;
    }

    // Funkce pro zpracovani formulare na hledani zakaznika
    public function hledatFormSucceeded($form) {
        // Nacteme data z formulare
        $values = $form->getValues();
        $q = $values;
        // Odesleme form s hodnotama
        $this->redirect("hledat", array(
            'firma' => $q->firma,
            'jmeno' => $q->jmeno,
            'prijmeni' => $q->prijmeni,
            'ic' => $q->ic,
            'dic' => $q->dic,
            'mesto' => $q->mesto,
            'ulice' => $q->ulice,
            'pozn' => $q->pozn,
            'web' => $q->web
        ));
    }

    // Formular na pridani zakaznika
    public function createComponentZakaznikForm() {
        $form = new Form();
        // Nastavime inputy
        $form->addText('firma', 'Firma')
                ->setAttribute('class', 'form-control');
        $form->addText('ic', 'IČ')
                ->setRequired('IČ musí být vyplněno!')
                ->setAttribute('class', 'form-control')
                ->addCondition(Form::FILLED)
                    ->addRule(Form::INTEGER, "IČ musí být číslo");
        $form->addText('dic', 'DIČ')
                ->setAttribute('class', 'form-control');
        $form->addText('titul_pred', 'Titlu před jménem')
                ->setAttribute('class', 'form-control');
        $form->addText('jmeno', 'Jméno')
                ->setAttribute('class', 'form-control');
        $form->addText('prijmeni', 'Přijmení')
                ->setAttribute('class', 'form-control');
        $form->addText('titul_po', 'Titul za jménem')
                ->setAttribute('class', 'form-control');
        $form->addText('ulice', 'Ulice')
                ->setAttribute('class', 'form-control');
        $form->addText('mesto', 'Město')
                ->setAttribute('class', 'form-control');
        $form->addText('psc', 'PČS')
                ->setAttribute('class', 'form-control');
        $form->addText('telefon', 'Telefon')
                ->setAttribute('class', 'form-control');
        $form->addText('mail', 'Email')
                ->setAttribute('class', 'form-control');
        $form->addText('web', 'Web')
                ->setAttribute('class', 'form-control');
        $form->addText('pozn', 'Poznámka')
                ->setAttribute('class', 'form-control');
        // Odesilaci tlacitko
        $form->addSubmit('send', 'Přidat zákazníka')
                ->getControlPrototype()
                ->class('btn btn-info');
        // Zavolame metodu usersFormSucceeded()po uspechu
        $form->onSuccess[] = $this->zakaznikFormSucceeded;
        // Set Twitter Bootstrap render
        $form->setRenderer(new BootstrapRenderer());
        return $form;
    }

    // Zpravovani formulare na pridani zakaznika
    public function zakaznikFormSucceeded($form) {
        // Nacteme hodnoty z formulare
        $id = $this->getParam("id");
        $values = $form->getValues();
        // Upravime uzivatele
        if ($id) {
            // Pokud ID existuje, nahradime data
            $this->zakaznici->findAll()->where('id', $id)->update($values);
            // Vypíšeme info a přesměrujeme
            $this->flashMessage("Zákazník byl úspěšně upraven!", "alert alert-success");
            $this->redirect("Zakaznici:default");
        } else {
            // Vytvorime uzivatele
            $this->zakaznici->insert(array(
                "firma" => $values->firma,
                "ic" => $values->ic,
                "dic" => $values->dic,
                "titul_pred" => $values->titul_pred,
                "jmeno" => $values->jmeno,
                "prijmeni" => $values->prijmeni,
                "titul_po" => $values->titul_po,
                "ulice" => $values->ulice,
                "mesto" => $values->mesto,
                "psc" => $values->psc,
                "telefon" => $values->telefon,
                "mail" => $values->mail,
                "web" => $values->web,
                "datum_ulozeni" => new Utils\DateTime,
                "pozn" => $values->pozn,
            ));
            // Vypíšeme info a přesměrujeme
            $this->flashMessage('Přidání zákazníka bylo úspěšné!', 'alert alert-success');
            $this->redirect('Zakaznici:default');
        }
    }

}
