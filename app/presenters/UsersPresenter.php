<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model,
    Nette\Application\UI\Form,
    Kdyby\BootstrapFormRenderer\BootstrapRenderer;

/**
 * Users presenter.
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */

class UsersPresenter extends HomepagePresenter {

    /**
     * @autowire
     * @var \App\Model\UsersModel
     */
    protected $users;

    // Predme $users do sablony
    public function renderDefault() {
        $this->template->users = $this->users->findAll();
    }

    // Signal pro smazani zaznamu
    public function handleSmazat($id) {
        $users = $this->users->findAll()->where("id", $id);
        if (!$users->count()) {
            $this->flashMessage("Uživatel nenalezen!", "alert alert-success");
            $this->redirect("this");
        }
        $users = $users->fetch();
        $users->delete();
        $this->flashMessage("Uživatel byl úspěšně odstřaněn!", "alert alert-success");
        $this->redirect("this");
    }

    // Signal na upravu uzivatele
    public function actionUpravit($id) {
        $users = $this->users->findAll()->where("id", $id);
        if (!$users->count())
            $this->redirect("default");
        $users = $users->fetch();
        
        // Nastavime defaultni hodnoty do formulare a nahradime button
        $this["usersForm"]->setDefaults($users);
        $this["usersForm"]['send']->caption = "Uložit změny";
        $this->template->users = $users;
    }

    // Vytvorime formular pro pridani/editaci uzivatelu
    protected function createComponentUsersForm() {
        $form = new Form();
        $form->addText('name', 'Jméno')
                ->setRequired('Jméno musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText('username', 'Uživatelské jméno')
                ->setRequired('Uživatelské jméno musí být vyplněno!')
                ->setAttribute('class', 'form-control');
        $form->addText('email', 'Email')
                ->setRequired('Email musí být vyplněn!')
                ->setAttribute('class', 'form-control');
        
        // Checkboxy pro nastaveni prav
        $form->addCheckbox('info_mail', '');
        $form->addCheckbox('projekt_prohlizeni', '');
        $form->addCheckbox('projekt_upravy', '');
        $form->addCheckbox('projekt_mazani', '');
        $form->addCheckbox('projekt_uzavirani', '');
        $form->addCheckbox('sprava_kalkulace', '');
        $form->addCheckbox('sprava_protokolu', '');
        $form->addCheckbox('zakaznici_prohlizeni', '');
        $form->addCheckbox('zakaznici_editace', '');
        $form->addCheckbox('zakaznici_statistika', '');
        $form->addCheckbox('sklad_prohlizeni', '');
        $form->addCheckbox('sklad_editace', '');
        $form->addCheckbox('rad_prohlizeni', '');
        $form->addCheckbox('rad_sprava', '');
        $form->addCheckbox('uzivatele', '');
        
        // Odesilaci tlacitko
        $form->addSubmit('send', 'Přidat uživatele')
                ->getControlPrototype()
                ->class('btn btn-info');

        // Zavolame metodu usersFormSucceeded()po uspechu
        $form->onSuccess[] = $this->usersFormSucceeded;
        
        // Set Twitter Bootstrap render
        $form->setRenderer(new BootstrapRenderer());
        
        return $form;
    }

    // Zpracovani formulare uzivatelu
    public function usersFormSucceeded($form) {
        // Nacteme hodnoty z formulare
        $id = $this->getParam("id");
        $values = $form->getValues();
        // Upravime uzivatele
        if ($id) {
            $this->users->findAll()->where('id', $id)->update($values);

            // Vypíšeme info a přesměrujeme
            $this->flashMessage("Uživatel byl úspěšně upraven!", "alert alert-success");
            $this->redirect("Users:default");
        } else {
            // Vytvorime uzivatele
            $this->users->insert(array(
                "username" => $values->username,
                "password" => md5($values->username),
                "name" => $values->name,
                "email" => $values->email,
                "info_mail" => $values->info_mail,
                "projekt_prohlizeni" => $values->projekt_prohlizeni,
                "projekt_upravy" => $values->projekt_upravy,
                "projekt_mazani" => $values->projekt_mazani,
                "projekt_uzavirani" => $values->projekt_uzavirani,
                "sprava_kalkulace" => $values->sprava_kalkulace,
                "sprava_protokolu" => $values->sprava_protokolu,
                "zakaznici_prohlizeni" => $values->zakaznici_prohlizeni,
                "zakaznici_editace" => $values->zakaznici_editace,
                "zakaznici_statistika" => $values->zakaznici_statistika,
                "sklad_prohlizeni" => $values->sklad_prohlizeni,
                "sklad_editace" => $values->sklad_editace,
                "rad_prohlizeni" => $values->rad_prohlizeni,
                "rad_sprava" => $values->rad_sprava,
                "uzivatele" => $values->uzivatele,
            ));

            // Vypíšeme info a přesměrujeme
            $this->flashMessage('Přidání uživatele bylo úspěšné!', 'alert alert-success');
            $this->redirect('Users:default');
        }
    }

}
