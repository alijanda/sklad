<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Model,
    Nette\Application\UI\Form,
    Kdyby\BootstrapFormRenderer\BootstrapRenderer,
    Vodacek\Forms\Controls\DateInput;

/**
 * Projekty presenter.
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */
class ProjektyPresenter extends HomepagePresenter {

    /**
     * @autowire
     * @var \App\Model\ProjektyModel
     */
    protected $projekty;

    // Aktualni den
    public function aktualDen() {
        return date('j');
    }

    // Aktualni mesic
    public function aktualMesic() {
        return date('n');
    }

    // Aktualni rok
    public function aktualRok() {
        return date('Y');
    }

    public function vypisProduktu() {
        $data = $this->projekty->findAll()
                ->where("start_date >= ?", $this->aktualRok() . "-01-01")
                ->where("end_date <= ?", $this->aktualRok() . "-12-31");
        return $data;
    }

    // Nacteni $projekty do sablny
    public function renderDefault() {
        $this->template->projekty = $this->vypisProduktu();
    }

    // Signal pro smazani zaznamu
    public function handleSmazat($id) {
        $projekty = $this->projekty->findAll()->where("id", $id);
        if (!$projekty->count()) {
            $this->flashMessage("Projekt nenalezen!", "alert alert-success");
            $this->redirect("this");
        }
        $projekty = $projekty->fetch();
        $projekty->delete();
        $this->flashMessage("Projekt byl úspěšně odstřaněn!", "alert alert-success");
        $this->redirect("this");
    }

    public function actionZobrazit($id) {
        $projekty = $this->projekty->findAll()->where("id", $id);
        if (!$projekty->count())
            $this->redirect("default");
        $projekty = $projekty->fetch();
        $this->template->projekty = $projekty;
    }

    // Signal na upravu zaznamu
    public function actionUpravit($id) {
        $projekty = $this->projekty->findAll()->where("id", $id);
        if (!$projekty->count())
            $this->redirect("default");
        $projekty = $projekty->fetch();

        // Nastavime defaultni hodnoty do formulare a nahradime button
        $this["projektyForm"]->setDefaults($projekty);
        //$this["projektyForm"]['send']->caption = "Uložit";
        $this->template->projekty = $projekty;
    }

    // Formular pro projekty
    public function createComponentProjektyForm() {
        $form = new Form();
        // Nastavime inputy pro Zakaznika
        $form->addText('firma', 'Firma')
                ->setAttribute('class', 'form-control');
        $form->addText('ic', 'IČ')
                ->setRequired('IČ musí být vyplněno!')
                ->setAttribute('class', 'form-control')
                ->addCondition(Form::FILLED)
                    ->addRule(Form::INTEGER, "IČ musí být číslo");
        $form->addText('dic', 'DIČ')
                ->setAttribute('class', 'form-control');
        $form->addText('titul_pred', 'Titlu před jménem')
                ->setAttribute('class', 'form-control');
        $form->addText('jmeno', 'Jméno')
                ->setAttribute('class', 'form-control');
        $form->addText('prijmeni', 'Přijmení')
                ->setAttribute('class', 'form-control');
        $form->addText('titul_po', 'Titul za jménem')
                ->setAttribute('class', 'form-control');
        $form->addText('ulice', 'Ulice')
                ->setAttribute('class', 'form-control');
        $form->addText('mesto', 'Město')
                ->setAttribute('class', 'form-control');
        $form->addText('psc', 'PČS')
                ->setAttribute('class', 'form-control');
        $form->addText('telefon', 'Telefon')
                ->setAttribute('class', 'form-control');
        $form->addText('mail', 'Email')
                ->setAttribute('class', 'form-control');
        $form->addText('web', 'Web')
                ->setAttribute('class', 'form-control');
        $form->addText('pozn', 'Poznámka')
                ->setAttribute('class', 'form-control');
        // nastavime inputy pro projekt
        $form->addText("nazev", "Název")
                ->setAttribute('class', 'form-control');
        $form->addText("nazev_stanku", "Název stánku")
                ->setAttribute('class', 'form-control');
        $form->addText("adresa_stanku", "Adresa stánku")
                ->setAttribute('class', 'form-control');
        $form->addText("cislo_stanku", "Číslo stánku")
                ->setAttribute('class', 'form-control');
        $form->addText("plocha_stanku", "Plocha stánku")
                ->setAttribute('class', 'form-control');
        $form->addDate('start_date', 'Start', DateInput::TYPE_DATETIME_LOCAL)
                ->setAttribute('class', 'form-control');
        $form->addDate('end_date', 'Start', DateInput::TYPE_DATETIME_LOCAL)
                ->setAttribute('class', 'form-control');
        $form->addDate('montaz_zacatek', 'Start', DateInput::TYPE_DATETIME_LOCAL)
                ->setAttribute('class', 'form-control');
        $form->addDate('demontaz_konec', 'Start', DateInput::TYPE_DATETIME_LOCAL)
                ->setAttribute('class', 'form-control');
        $form->addTextArea("popis", "Popis:", 10, 10)
                ->setAttribute('class', 'form-control');
        $form->addRadioList("stav", "",array(
            "0" => "Neschváleno",
            "1" => "Připravuje se",
            "2" => "Možné realizovat",
            "3" => "Uzavřeno"
        ));
        $form->addUpload("dokumenty","dokumenty", true)
                ->setAttribute('class', 'form-control');
        $form->addUpload("navrh")
                ->setAttribute('class', 'form-control');
        $form->addUpload("hotovy")
                ->setAttribute('class', 'form-control');
        $form->addUpload("prvek")
                ->setAttribute('class', 'form-control');
        $form->addSubmit("send", "Založit projekt")
                ->getControlPrototype()
                ->class('btn btn-info');

        return $form;
    }

}
