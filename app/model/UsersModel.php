<?php

namespace App\Model;

use Nette;

/**
 * Model pro praci s uživateli
 * @author Artao.cz <info@artao.cz>
 * @package Papst.cz
 */
class UsersModel extends \Nette\Object {

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    /** @return Nette\Database\Table\Selection */
    public function findAll() {
        return $this->database->table('users');
    }

    /** @return Nette\Database\Table\ActiveRow */
    public function findById($id) {
        return $this->findAll()->get($id);
    }

    /** @return Nette\Database\Table\ActiveRow */
    public function insert($values) {
        return $this->findAll()->insert($values);
    }

    /** $return Nette\Database\Table\ActioveRow */
    public function updated($values) {
        return $this->findAll()->where($id)->update($values);
    }

}
