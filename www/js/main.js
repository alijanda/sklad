
$(document).ready(function() {
    $(function() {
        $(".tablesorter").tablesorter(
                {
                    theme: 'blue',
                    sortList: [[1, 0], [2, 0], [3, 0]],
                    // header layout template; {icon} needed for some themes
                    headerTemplate: '{content}{icon}',
                    // initialize column styling of the table
                    widgets: ["columns"],
                    widgetOptions: {
                        // change the default column class names
                        // primary is the first column sorted, secondary is the second, etc
                        columns: ["primary", "secondary", "tertiary"]
                    }
                });
    });
});

$(document).ready(function() {
    $('input[data-dateinput-type]').dateinput({
        datetime: {
            dateFormat: 'd.m.yy',
            timeFormat: 'H:mm',
            options: {// options for type=datetime
                changeYear: true
            }
        },
        'datetime-local': {
            dateFormat: 'd.m.yy',
            timeFormat: 'H:mm'
        },
        date: {
            dateFormat: 'd.m.yy'
        },
        month: {
            dateFormat: 'MM yy'
        },
        week: {
            dateFormat: "w. 'week of' yy"
        },
        time: {
            timeFormat: 'H:mm'
        },
        options: {// global options
            closeText: "Close"
        }
    });
});